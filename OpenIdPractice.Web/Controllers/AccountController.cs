﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace OpenIdPractice.Web.Controllers
{
    public class AccountController:Controller
    {
        public ActionResult Login()
        {
            return View("Login");
        }

        [HttpPost]
        public ActionResult Login(string userName, string password)
        {
            if (userName == "richie")
            {
                FormsAuthentication.SetAuthCookie("richie", false);
                return RedirectToAction("Orders","Account");
            }

            return View("Login");
        }

        [Authorize]
        public ActionResult Orders()
        {
            return View("Orders");
        } 

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Login", "Account");
        }
    }
}